#include "Windows.h"
#include <stdio.h>
#include <ddraw.h>
#include <cstdint>
#include <cmath>
#include <cassert>
// Refactor these structs into separate headers
// Structs
struct Time {
  float deltaTime = 0;
};

struct v2 {
  float x = 0;
  float y = 0;
};

struct Vec3 {
float x;
float y;
float z;

};

struct Line {
    Vec3 start;
    Vec3 end;
};

struct GameData {
  
};


struct GameVM {
  uint8_t heap[1024];
  uint8_t stack[256];
};

struct AssemblyProgram {
  int numberOfLines;
  char** lines;
};

struct BinaryProgram {
  int codeLengthInBytes;
  uint8_t* code;

};

struct OpDesc {
  int numBytes;
  uint8_t* bytes;
};



// MACROS
#define INIT_DDSD \
  DDSURFACEDESC2 ddsd; \
  memset(&ddsd, 0, sizeof(ddsd));\
  ddsd.dwSize = sizeof(ddsd);

// END MACROS

// Globals
LPDIRECTDRAW lpdd_temp = nullptr;
LPDIRECTDRAW4 lpdd = nullptr;
LPDIRECTDRAWSURFACE4 lpddsprimary = nullptr;
LPDIRECTDRAWSURFACE4 lpddsback = nullptr;
Time time = {};
GameData gameData = {};
// End globals

// Function prototypes
void plotPixel(int x, int y, float r, float g, float b, float a, long pitch, uint8_t* pixels);
uint8_t* lockBackBuffer(long* outPitch);
void unlockBackBuffer();

AssemblyProgram* loadProgramFromFile(const char* fileName);
BinaryProgram* assembleProgram(AssemblyProgram*);

// END Function prototypes

// This function receives the current binary and just 
// appends the correspoding byte code to the binary. 


uint8_t getOpCodeFromString(char* opcodeString) {
  if (strcmp(opcodeString, "pushi") == 0) return 0x01;
  else if (strcmp(opcodeString, "popi") == 0) return 0x02;
  else return 0x00;

}



uint8_t readByteOperandFromString(const char* numberString) {

  int number = atoi(numberString);
  if (number > 255) {
    exit(1);
  }
  else 
  {
    return (uint8_t) number;
  }



}

OpDesc parseLine(uint8_t* binary, char* line) {
  OpDesc opDesc = {};
  char c = ' ';
  char *opWord = new char[155];
  while (c != ' ') {
    *opWord = c;
    opWord++;
  }

  if (strcmp("pushi", opWord) == 0) {
    uint8_t* bytes = new uint8_t[2];
    bytes[0] = getOpCodeFromString("pushi");
    // opWord shoulad actually point to the correct position 
    // where the operand comes next.
    bytes[1] = readByteOperandFromString(opWord);
    opDesc.numBytes = 2;
    opDesc.bytes = bytes;
  
  }

  delete(opWord);
  return opDesc;

  
}


BinaryProgram* assembleProgram(AssemblyProgram* assemblyProgram) {
  // Lets start with a 64kb block of code :) 
  uint8_t* binary = (uint8_t*) malloc(64 * 1024) ;
  char* line = nullptr;
  for (int i = 0; i < assemblyProgram->numberOfLines;i++) {
    line = assemblyProgram->lines[i];

  }
  BinaryProgram* binaryProgram = new BinaryProgram();
  // TODO use the real/actual length here?!
  binaryProgram->codeLengthInBytes = 64 * 1024; 
  binaryProgram->code = binary; 
  return binaryProgram;

}
// This function returns an array of 
// Strings representing the lines of the assembly program.
AssemblyProgram* loadProgramFromFile(const char* fileName) {
  FILE* file = fopen(fileName, "rb");
  fseek(file, 0, SEEK_END);
  long size = ftell(file);
  rewind(file);

  // Count the lines
  int lineCount = 0;
  bool countDone = false;
  while (!countDone) {
    char c=' ';
    while (c != '\n') {
      int r = fread(&c, 1, 1, file);
      if (r == EOF || r == 0){
        countDone = true;
        break;
      }
    }
    lineCount++;
  }
  
  rewind(file);

  char** buffer = (char**)malloc(lineCount);
  bool done = false;
  int lineIndex = 0; 
  while (!done) {
    char* lineBuffer = new char[255];
    memset(lineBuffer, 0, 255);
    char c=' ';
    int charIndex = 0;
    bool lineDone = false;
    while (!lineDone) {
      int r = fread(&c, 1, 1, file);
      if (c != '\r' && r != 0) {
        lineBuffer[charIndex++] = c;
      } 
      else {
        // Swallow the carriage return/linefeed.
        fread(&c, 1, 1, file);
        lineDone = true;
      }
      if (r == 0) {
        done = true;
        break;
      } 
    }
    buffer[lineIndex++] = lineBuffer;
  }
  AssemblyProgram* ap = new AssemblyProgram();
  ap->numberOfLines = lineCount;
  ap->lines = buffer;
  return ap;
}




LPDIRECTDRAWSURFACE4 createOffscreenSurface(int w, int h) {
  LPDIRECTDRAWSURFACE4 lpdds= nullptr;
  DDSURFACEDESC2 ddsd;
  memset(&ddsd, 0, sizeof(ddsd));
  ddsd.dwFlags = DDSD_CAPS | DDSD_WIDTH | DDSD_HEIGHT;
  ddsd.dwWidth = w;
  ddsd.dwHeight = h;
  ddsd.ddsCaps.dwCaps = DDSCAPS_OFFSCREENPLAIN | DDSCAPS_VIDEOMEMORY;

  HRESULT hr = lpdd->CreateSurface(&ddsd, &lpdds, NULL);
  if (FAILED(hr)) {
    exit(0);
    return nullptr;
  }
  return lpdds;

}

void drawYDominantLine(v2 start, v2 end, float gradient, long pitch, uint8_t* pixels) {

  float dx = end.x - start.x;
  float dy = end.y - start.y; 
  gradient = abs(dx) / abs(dy);
  if (dx < 0) {
    gradient = -gradient;
  }
  
 

  if (end.y > start.y) {
    float x = start.x;
    for (int y = start.y; y<end.y; y++) {
      plotPixel((int)x, y, 1, 1, 0, 1, pitch, pixels);
      x += gradient;
    }
  }
  else {
    float x = start.x;
    for (int y = start.y; y>end.y; y--) {
      plotPixel((int)x, y, 1, 1, 0, 1, pitch, pixels);
      x += gradient;
    }
  }

}

void drawXDominantLine(v2 start, v2 end, float gradient, long pitch, uint8_t* pixels) {


  if (end.x < start.x) {

    v2 temp;
    temp.x = start.x;
    temp.y = start.y;
    start.x = end.x;
    start.y = end.y;
    end.x = temp.x;
    end.y = temp.y;
  }

  float y = start.y + gradient;
  for (int x = start.x; x < end.x; x++) {
    plotPixel(x, y, 1, 1, 0, 1, pitch, pixels);
    y += gradient;
  }
}

void drawLine(v2 start, v2 end, long pitch, uint8_t* pixels) {


  float dx = end.x - start.x;
  float dy = end.y - start.y; 
  float gradient = dy / dx;

  if (abs(dy) > abs(dx)) {
    drawYDominantLine(start, end, gradient, pitch, pixels);
  }

  if (abs(dy) <= abs(dx)) {
    drawXDominantLine(start, end, gradient, pitch, pixels);
  }

}

void plotPixel(int x, int y, float r, float g, float b, float a, long pitch, uint8_t* pixels) {

  // Need to multiply our x coordinate by 4 as we move in 32bits (= 4 byte) increments
  int startIndex = x* 4 + y * pitch;
  pixels[startIndex + 0] = b * (float)255;
  pixels[startIndex + 1] = g * (float)255;
  pixels[startIndex + 2] = r * (float)255;
  pixels[startIndex + 3] = a * (float)255;

}


void initDirectX(HWND hwnd) {
  if (FAILED(DirectDrawCreate(NULL, &lpdd_temp, NULL))) {
    return; 
  }

  if (FAILED(lpdd_temp->QueryInterface(IID_IDirectDraw4, (LPVOID*) &lpdd))) {
    return;

  }
  lpdd_temp->Release();
  lpdd_temp = nullptr;

  HRESULT hr = lpdd->SetCooperativeLevel(hwnd, 
      DDSCL_FULLSCREEN | DDSCL_ALLOWMODEX | 
      DDSCL_EXCLUSIVE | DDSCL_ALLOWREBOOT );
  if (FAILED(hr)) {
    return;
  }

  hr = lpdd->SetDisplayMode(800, 600, 32, 0, 0);  
  if (FAILED(hr)) {
    return;
  }

  DDSURFACEDESC2 ddsd;
  memset(&ddsd, 0, sizeof(ddsd));

  ddsd.dwSize = sizeof(ddsd);
  ddsd.dwFlags = DDSD_CAPS | DDSD_BACKBUFFERCOUNT;
  ddsd.dwBackBufferCount = 1;
  ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE | DDSCAPS_COMPLEX | DDSCAPS_FLIP;
  
  hr = lpdd->CreateSurface(&ddsd, &lpddsprimary, NULL);
  if (FAILED(hr)) {
    return;
  }
  ddsd.ddsCaps.dwCaps = DDSCAPS_BACKBUFFER;
  hr = lpddsprimary->GetAttachedSurface(&ddsd.ddsCaps, &lpddsback);
  if (FAILED(hr)) {
    return;
  }


}

static void clear() {

  DDSURFACEDESC2 ddsd;
  memset(&ddsd, 0, sizeof(ddsd));
  ddsd.dwSize = sizeof(ddsd);

 // HRESULT hr = lpddsback->Lock(NULL, &ddsd, DDLOCK_SURFACEMEMORYPTR | DDLOCK_WAIT, NULL);

  DDBLTFX bltfx;
  ZeroMemory(&bltfx, sizeof(bltfx));
  bltfx.dwSize = sizeof(bltfx);

  // A R G B
  bltfx.dwFillColor = (int) 255 << 24 | 0 << 16 | 0 << 8 | 0;
  RECT dr;
  dr.left = 0;
  dr.top = 0;
  dr.bottom = 600;
  dr.right = 800;


  lpddsback->Blt(&dr, 0, 0, DDBLT_COLORFILL | DDBLT_WAIT, &bltfx);

  /*if (FAILED(hr)) {

    return;
  }
  uint8_t* pixels = (uint8_t*) ddsd.lpSurface;

  for (int i = 0; i < 800 * 600 * 4; i+=4) {
    int startIndex = i;
    pixels[startIndex + 0] = 0;
    pixels[startIndex + 1] = 0;
    pixels[startIndex + 2] = 255;
    pixels[startIndex + 3] = 255;

  }


  lpddsback->Unlock(NULL);
  */

}


static LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    HDC hdc;
    RECT rect;
    TEXTMETRIC tm;
    PAINTSTRUCT ps;

    switch (message) {

    case WM_CREATE:
    {
        //initGL(hwnd);
        break;

    }
   

    case WM_CLOSE:
        /* if (MessageBox(NULL, "Are you sure you want to quit?",
                        "Confirmation", MB_ICONQUESTION | MB_YESNO) == IDYES)*/
        DestroyWindow(hwnd);
        break;

    case WM_DESTROY:
        PostQuitMessage(0);
        break;

    case WM_PAINT: {

        hdc = BeginPaint(hwnd, &ps);
        clear();
        EndPaint(hwnd, &ps);

        break;
    }
    default:
        return DefWindowProc(hwnd, message, wParam, lParam);
    }
    return 0;
}

void transformationPipeline() {

}

void update(float frameTime) {
  // TODO add the interpretation of our virtual machine here
}


uint8_t* lockBackBuffer(long* outPitch) {
  INIT_DDSD
  if (FAILED(lpddsback->Lock(NULL, &ddsd, DDLOCK_SURFACEMEMORYPTR | DDLOCK_WAIT, NULL))) {
    exit(1);
  }
  *outPitch = (LONG) ddsd.lPitch;
  return (uint8_t*) ddsd.lpSurface;
}

void unlockBackBuffer() {
  lpddsback->Unlock(NULL);
}

void render(float frameTime) {
      clear();
      //plotPixel(400, 300, 0, 0.5, 1, 1);
      long pitch;
      uint8_t* pixels = lockBackBuffer(&pitch);


//      drawLine({ 400, 300}, {600, 200}, pitch, pixels);
//      drawLine({ 400, 300}, {400, 10}, pitch, pixels);
//      drawLine({ 400, 300}, {700, 400}, pitch, pixels);
//      drawLine({ 400, 300}, {600, 550}, pitch, pixels);
//      drawLine({ 400, 300}, {400, 550}, pitch, pixels);
//      drawLine({ 400, 300}, {100, 550}, pitch, pixels);
//      drawLine({ 400, 300}, {200, 350}, pitch, pixels);
//      drawLine({ 400, 300}, {0, 300}, pitch, pixels);
//      drawLine({ 400, 300}, {20, 150}, pitch, pixels);
//      drawLine({ 400, 300}, {0, 50}, pitch, pixels);
//      drawLine({ 400, 300}, {200, 50}, pitch, pixels);
//      drawLine({ 50, 10}, {300, 10}, pitch, pixels);
//      drawLine({ 50, 10}, {50, 100}, pitch, pixels);
//      drawLine({ 650, 100}, {50, 10}, pitch, pixels);
      drawLine({ 50, 500}, {150, 10}, pitch, pixels);
      drawLine({200, 100}, {300, 400}, pitch, pixels);
      unlockBackBuffer();

}

void assemblyTests() {
  uint8_t num1 = readByteOperandFromString("255");
  assert(num1 == 255);
}

void mainLoop() {

    LARGE_INTEGER freq;
    QueryPerformanceFrequency(&freq);
    LARGE_INTEGER start;
    LARGE_INTEGER end;
    start.QuadPart = 0;
    LARGE_INTEGER elapsed;
    elapsed.QuadPart = 0;


    AssemblyProgram* assemblyProgram = loadProgramFromFile("myprog.txt");
    assemblyTests();
    


    bool running = true;

    MSG msg;
    while (running) {

      if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE) > 0) {

        if (msg.message == WM_QUIT) {
          running = false;
        
        }

        TranslateMessage(&msg);	
        DispatchMessage(&msg);
      }


      QueryPerformanceCounter(&start);

      update(elapsed.QuadPart);


      render(elapsed.QuadPart);

      QueryPerformanceCounter(&end);
      elapsed.QuadPart = end.QuadPart - start.QuadPart;
      elapsed.QuadPart *= 1000000;
      elapsed.QuadPart /= freq.QuadPart;
      char buf[200];
      sprintf_s(buf, 200, "frametint: %ld\n", (long)elapsed.QuadPart);
      OutputDebugString(buf);

      while (FAILED(lpddsprimary->Flip(NULL, DDFLIP_WAIT)));

    }	

}

int APIENTRY WinMain(
                HINSTANCE hInstance,
                HINSTANCE hPrevInstance,
                LPSTR lpCmdLine,
                int nShowCmd) 
{



  const char g_szClassName[] = "winClass";
        WNDCLASSEX wc;

        wc.lpszMenuName = NULL;
        wc.hInstance = hInstance;
        wc.lpszClassName = g_szClassName;
        wc.cbSize = sizeof(WNDCLASSEX);
        wc.cbClsExtra = wc.cbWndExtra = 0;
        wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
        wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
        wc.hCursor = LoadCursor(NULL, IDC_ARROW);
        wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
        wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
        wc.lpfnWndProc = WndProc;

        if (!RegisterClassEx(&wc)) {
            MessageBox(NULL, "Window Registration Failed", "Error", MB_ICONEXCLAMATION | MB_OK);
            exit(1);
        }

        //RECT corrRect = { 0, 0, _winCreateParams.width, _winCreateParams.height};
				RECT corrRect = { 0, 0, 800, 600};

        bool ok0 = AdjustWindowRect(&corrRect, WS_OVERLAPPEDWINDOW, false);
 
				HWND hwnd = CreateWindow(
            g_szClassName,
            "XRender WIN32 window",
            WS_POPUP ,
            //WS_OVERLAPPEDWINDOW,
            //400, 300, corrRect.right - corrRect.left, corrRect.bottom - corrRect.top,
            400, 300, 800, 600,
            NULL, NULL, hInstance, NULL);

        initDirectX(hwnd);

        if (hwnd == NULL) {
            MessageBox(NULL, "Window Creation Failed", "Error", MB_ICONEXCLAMATION | MB_OK);
            return 0;
        }

        ShowWindow(hwnd, SW_NORMAL);
        UpdateWindow(hwnd);

        mainLoop();
      

  return 0;

}
